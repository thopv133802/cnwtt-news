import {ADD_USER, AddUser, AuthAction, SET_CURRENT_USER, UPDATE_USER, UpdateUser} from "../actions/auth.action"
import {fakeAdminUser, fakeMemberUser, User} from "../vos/auth.vo"

export interface AuthState{
  currentUser: User,
  users: User[]
}

const users = [
  fakeAdminUser("jlaotsezu", "123456"),
  fakeMemberUser("thopvna", "123456")
]

const initialState = {
  currentUser: users[0],
  users: users
}

export function authReducer(state: AuthState = initialState, action: AuthAction){
  switch (action.type) {
    case ADD_USER:
      return {
        currentUser: state.currentUser,
        users: [
          (<AddUser>action).user,
          ...state.users
        ]
      }
    case UPDATE_USER:
      return {
        currentUser: state.currentUser,
        users: [
          (<UpdateUser>action).user,
          ...state.users.filter(user => user.username !== action.user.username)
        ]
      }
    case SET_CURRENT_USER:
      return {
        currentUser: action.user,
        users: state.users
      }
    default:
      return state
  }
}

export const howToExtractUsers = (state: AuthState) => {
  return state.users
}
export const howToExtractCurrentUser = (state: AuthState) => {
  return state.currentUser
}
