import {
  ADD_CATEGORIES,
  ADD_CATEGORY,
  ADD_NEWS,
  AddCategories,
  AddCategory, AddNews,
  NewsActions,
  UPDATE_NEWS,
  UpdateNews
} from "../actions/news.actions"
import {Category, fakeCategories, fakeCategory, fakeNews, fakeNewses, fakeNewsWithCategory, News, NewsComment} from "../vos/news.vo"
import {Action} from "@ngrx/store"

export interface NewsState{
  newses: News[],
  categories: Category[]
}

const categories = [
  ...fakeCategories()
]

const newses = [
  ...fakeNewses(categories)
]

const initialState: NewsState = {
  newses: newses,
  categories: categories
}

export function newsReducer(state: NewsState = initialState, action: NewsActions): NewsState{
  switch (action.type) {
    case ADD_CATEGORIES:
      return {
        newses: state.newses,
        categories: [
          ...(<AddCategories>action).categories,
          ...state.categories
        ]
      }
    case ADD_CATEGORY:
      return{
        newses: state.newses,
        categories: [
          (<AddCategory>action).category,
          ...state.categories
        ]
      }
    case UPDATE_NEWS:
      const updatedNews = (<UpdateNews>action).news
      return {
        newses: [
          updatedNews,
          ...state.newses.filter(news => news.id !== updatedNews.id)
        ],
        categories: state.categories
      }
    case ADD_NEWS:
      const addedNews = (<AddNews>action).news
      return {
        categories: state.categories,
        newses: [
          addedNews,
          ...state.newses
        ]
      }
    default:
      return state
  }
}

export const howToExtractCategories = (state: NewsState) => {
  return state.categories
}
export const howToExtractNewses = (state: NewsState) => {
  return state.newses
}

