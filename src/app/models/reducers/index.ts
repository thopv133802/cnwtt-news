import {
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../../environments/environment';
import {howToExtractCategories, howToExtractNewses, newsReducer, NewsState} from "./news.reducer"
import {authReducer, AuthState, howToExtractCurrentUser, howToExtractUsers} from "./auth.reducer"
import {howToExtractComments, newsCommentReducer, NewsCommentState} from "./news-comment.reducer"
import {howToExtractPostComments, howToExtractPosts, postReducer, PostState} from "./post.reducer"

export interface RootState {
  news: NewsState,
  auth: AuthState,
  newsComment: NewsCommentState,
  post: PostState
}

export const reducers: ActionReducerMap<RootState> = {
  news: newsReducer,
  auth: authReducer,
  newsComment: newsCommentReducer,
  post: postReducer
};


export const metaReducers: MetaReducer<RootState>[] = !environment.production ? [] : [];

export const fetchNewsState = createFeatureSelector("news")
export const fetchCategories = createSelector(fetchNewsState, howToExtractCategories)
export const fetchNewses = createSelector(fetchNewsState, howToExtractNewses)

export const fetchAuthState = createFeatureSelector("auth")
export const fetchCurrentUser = createSelector(fetchAuthState, howToExtractCurrentUser)
export const fetchUsers = createSelector(fetchAuthState, howToExtractUsers)


export const fetchNewsCommentState = createFeatureSelector("newsComment")
export const fetchNewsComments = createSelector(fetchNewsCommentState, howToExtractComments)


export const fetchPostState = createFeatureSelector("post")
export const fetchPosts = createSelector(fetchPostState, howToExtractPosts)
export const fetchPostComments = createSelector(fetchPostState, howToExtractPostComments)
