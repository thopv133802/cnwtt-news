import {ADD_POST, ADD_POST_COMMENT, AddPost, AddPostComment, PostActions, UPDATE_POST, UpdatePost} from "../actions/post.actions"
import {fakePosts, Post, PostComment} from "../vos/post.vo"

export interface PostState{
  posts: Post[],
  comments: PostComment[]
}
export const initialPostState = {
  posts: fakePosts(),
  comments: []
}

export function postReducer(state: PostState = initialPostState, action: PostActions): PostState{
  switch (action.type) {
    case ADD_POST:
      return {
        posts: [
          (<AddPost>action).post,
          ...state.posts
        ],
        comments: state.comments
      }
    case UPDATE_POST:
      const updatedPost = (<UpdatePost>action).post
      return {
        posts: [
          updatedPost,
          ...state.posts.filter(post => post.id !== updatedPost.id)
        ],
        comments: state.comments
      }
    case ADD_POST_COMMENT:
      const comment = (<AddPostComment>action).comment
      return {
        posts: state.posts,
        comments: [
          comment,
          ...state.comments
        ]
      }
    default:
      return state
  }
}


export const howToExtractPosts = (state: PostState) => {
  return state.posts
}
export const howToExtractPostComments = (state: PostState) => {
  return state.comments
}
