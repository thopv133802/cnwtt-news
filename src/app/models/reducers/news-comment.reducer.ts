import {NewsComment} from "../vos/news.vo"
import {ADD_COMMENT, NewsCommentAction} from "../actions/news-comment.action"


export interface NewsCommentState{
  comments: NewsComment[]
}

const initialState = {
  comments: []
}

export function newsCommentReducer(state: NewsCommentState = initialState, action: NewsCommentAction): NewsCommentState{
  switch (action.type) {
    case ADD_COMMENT:
      return {
        comments: [
          action.comment,
          ...state.comments
        ]
      }
    default:
      return state
  }
}

export const howToExtractComments = (state: NewsCommentState) => {
  return state.comments
}
