import {Action} from "@ngrx/store"
import {NewsComment} from "../vos/news.vo"

export const ADD_COMMENT = "news-comment addComment"

export class AddComment implements Action{
  type = ADD_COMMENT
  comment: NewsComment


  constructor(comment: NewsComment) {
    this.comment = comment
  }
}

export type NewsCommentAction = AddComment
