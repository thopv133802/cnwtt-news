import {Action} from "@ngrx/store"
import {Category, News} from "../vos/news.vo"

export const ADD_CATEGORIES = "[news] add_categories"
export const ADD_CATEGORY = "[news] add_category"
export const ADD_NEWSES = "[news] add_newses"
export const ADD_NEWS = "[news] add_news"
export const UPDATE_NEWS = "[news] update_news"

export class AddCategories implements Action{
  type = ADD_CATEGORIES
  categories: Category[]

  constructor(categories: Category[]) {
    this.categories = categories
  }
}
export class AddCategory implements  Action{
  type = ADD_CATEGORY
  category: Category

  constructor(category: Category) {
    this.category = category
  }
}
export class AddNewses implements Action{
  type = ADD_NEWSES
  newses: News[]

  constructor(newses: News[]) {
    this.newses = newses
  }
}
export class AddNews implements Action{
  type = ADD_NEWS
  news: News

  constructor(news: News) {
    this.news = news
  }
}

export class UpdateNews implements Action{
  type = UPDATE_NEWS
  news: News

  constructor(news: News) {
    this.news = news
  }
}

export type NewsActions = AddCategories | AddCategory | AddNewses | AddNews | UpdateNews
