import {Action} from "@ngrx/store"
import {User} from "../vos/auth.vo"

export const ADD_USER = "auth add_user"
export const UPDATE_USER = "auth update_user"
export const SET_CURRENT_USER = "auth set_current_user"

export class AddUser implements Action{
  type = ADD_USER
  user: User

  constructor(user: User) {
    this.user = user
  }
}

export class UpdateUser implements Action{
  type = UPDATE_USER
  user: User

  constructor(user: User) {
    this.user = user
  }
}
export class SetCurrentUser implements Action{
  type = SET_CURRENT_USER
  user: User
  constructor(user: User){
    this.user = user
  }
}

export type AuthAction = AddUser | UpdateUser
