import {Action} from "@ngrx/store"
import {Post, PostComment} from "../vos/post.vo"

export const ADD_POST = "post addpost"
export const UPDATE_POST = "post updatepost"
export const ADD_POST_COMMENT = "post updatepostcomment"

export class AddPost implements Action{
  type = ADD_POST
  post: Post

  constructor(post: Post) {
    this.post = post
  }
}

export class UpdatePost implements  Action{
  type = UPDATE_POST
  post: Post

  constructor(post: Post) {
    this.post = post
  }
}

export class AddPostComment implements Action{
  type = ADD_POST_COMMENT
  comment: PostComment

  constructor(comment: PostComment) {
    this.comment = comment
  }
}

export type PostActions = AddPost | UpdatePost | AddPostComment
