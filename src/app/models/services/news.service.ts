import { Injectable } from '@angular/core';
import {EMPTY, Observable, of, throwError, zip} from "rxjs"
import {Category, CategoryNewses, createEmptyNews, createNewsComment, fakeNews, News} from "../vos/news.vo"
import {select, Store} from "@ngrx/store"
import {fetchCategories, fetchCurrentUser, fetchNewsComments, fetchNewses, RootState} from "../reducers"
import {first, flatMap, map} from "rxjs/operators"
import {AuthService} from "./auth.service"
import {AddComment} from "../actions/news-comment.action"
import {AddCategory, AddNews, UpdateNews} from "../actions/news.actions"

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  constructor(private store: Store<RootState>, private authService: AuthService) {

  }

  fetchCategories(): Observable<Category[]> {
      return this.store.pipe(
        select(fetchCategories)
      )
  }

  fetchNewses(): Observable<News[]> {
    return this.store.pipe(
      select(fetchNewses)
    )
  }

  fetchCategoriesNewses(): Observable<CategoryNewses[]> {
    return this.fetchCategories()
      .pipe(
        flatMap(categories => {
          return this.fetchNewses()
            .pipe(first())
            .pipe(
              map(newses => {
                return categories.map(category => {
                  return {
                    category: category,
                    newses: newses.filter(news => news.categoryId === category.id)
                  }
                })
              })
            )
        })
      )
  }

  fetchNews(newsId: string): Observable<News> {
    return this.fetchNewses()
      .pipe(
        map(newses => newses.find(news => news.id === newsId))
      )
  }

  fetchNewsesByCategory(categoryId: string): Observable<News[]> {
    return this.fetchNewses()
      .pipe(
        map(newses => newses.filter(news => news.categoryId === categoryId))
      )
  }

  fetchComments(newsId: string) {
    return this.store.pipe(
      select(fetchNewsComments)
    )
      .pipe(
        map(comments => comments.filter(comment => comment.newsId === newsId))
      )
  }
  addComment(newComment: string, newsId): Observable<never> {
    return this.store.pipe(
      select(fetchCurrentUser)
    ).pipe(
      first()
    )
      .pipe(
        flatMap(user => {
          if (user == null){
            return throwError("Người dùng hiện tại không khả dụng")
          }
          else {
            this.store.dispatch(
              new AddComment(
                createNewsComment(newComment, newsId, user.username, user.fullName, user.thumbnail)
              )
            )
            return EMPTY
          }
        })
      )
  }

  updateNews(news: News): Observable<never> {
    this.store.dispatch(
      new UpdateNews(news)
    )
    return EMPTY
  }

  createNews(news: News): Observable<never>{
    return this.store.pipe(
      select(fetchCategories)
    )
      .pipe(
        first()
      )
      .pipe(
        map(categories => categories.find(category => category.name === news.categoryName))
      )
      .pipe(
        map(category => {
          if (category == null){
            category = {
              id: news.categoryId,
              name: news.categoryName,
              created: news.created
            }
            this.store.dispatch(
              new AddCategory(category)
            )
          }
          return category
        })
      )
      .pipe(
        flatMap(category => {
          news.categoryId = category.id
          news.categoryName = category.name
          this.store.dispatch(
            new AddNews(news)
          )
          return EMPTY
        })
      )
  }

  createEmptyNews(): Observable<News> {
    return this.authService.fetchCurrentUser()
      .pipe(first())
      .pipe(
        flatMap(user => {
          if (user != null) {
            return of(createEmptyNews(user.username))
          }
          else {
            return throwError("Người dùng hiện tại không khả dụng")
          }
        })
      )
  }
}
