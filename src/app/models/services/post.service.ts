import {Injectable} from "@angular/core"
import {fetchPostComments, fetchPosts, RootState} from "../reducers"
import {select, Store} from "@ngrx/store"
import {createNewPost, createNewPostComment, Post, PostComment} from "../vos/post.vo"
import {EMPTY, Observable} from "rxjs"
import {first, flatMap, map} from "rxjs/operators"
import {AuthService} from "./auth.service"
import {UserRole} from "../vos/auth.vo"
import {AddPost, AddPostComment, UpdatePost} from "../actions/post.actions"
import {AddComment} from "../actions/news-comment.action"

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private store: Store<RootState>, private authService: AuthService) { }

  fetchPosts(): Observable<Post[]> {
    return this.store.pipe(
      select(fetchPosts)
    )
  }

  fetchCountPostComments(postId: string): Observable<number> {
    return this.store.pipe(
      select(fetchPostComments)
    )
      .pipe(
        map(comments => comments.filter(comment => comment.postId === postId).length)
      )
  }

  fetchPost(id: string): Observable<Post> {
    return this.fetchPosts().pipe(
      map(posts => posts.find(post => post.id === id))
    )
  }

  currentUserEditable(post: Post): Observable<boolean> {
    return this.authService.fetchCurrentUser()
      .pipe(
        map(user => user.role === UserRole.Admin || user.username === post.creatorId)
      )
  }

  updatePost(post: Post): Observable<never> {
    this.store.dispatch(
      new UpdatePost(post)
    )
    return EMPTY
  }

  fetchComments(postId: string): Observable<PostComment[]> {
    return this.store.pipe(
      select(fetchPostComments)
    ).pipe(
      map(comments => comments.filter(comment => comment.postId === postId))
    )
  }

  addComment({content, postId}): Observable<never> {
    return this.authService.fetchCurrentUser()
      .pipe(
        first()
      )
      .pipe(
        flatMap(user => {
          const comment = createNewPostComment(content, user.username, user.fullName, user.thumbnail, postId)
          this.store.dispatch(
            new AddPostComment(comment)
          )
          return EMPTY
        })
      )
  }

  increaseViewsAmount(id: string) {
    this.fetchPost(id).pipe(first())
      .subscribe(post => {
        post.views += 1
        this.store.dispatch(
          new UpdatePost(post)
        )
      })
  }

  createPost(params: {content: string[], title: string}): Observable<never> {
    return this.authService.fetchCurrentUser().pipe(
      first()
    ).pipe(
      flatMap(user => {
        const post = createNewPost(params.title, params.content, user.username, user.fullName)
        this.store.dispatch(
          new AddPost(post)
        )
        return EMPTY
      })
    )
  }
}
