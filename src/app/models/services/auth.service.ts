import {Injectable} from "@angular/core"
import {fetchCurrentUser, fetchUsers, RootState} from "../reducers"
import {select, Store} from "@ngrx/store"
import {AddUser, SetCurrentUser, UpdateUser} from "../actions/auth.action"
import {UserRole, User, createNewUser, UserProfile} from "../vos/auth.vo"
import {EMPTY, Observable, throwError} from "rxjs"
import {first, flatMap, map} from "rxjs/operators"

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private store: Store<RootState>) {

  }

  register(params: { birthday: string; password: string; phone: string; fullName: string; email: string; username: string; thumbnail: string; adderId: string }): Observable<never> {
    const newUser: User = createNewUser(params)
    return this.store.pipe(
      select(fetchUsers)
    )
      .pipe(
        first()
      )
      .pipe(
        map(users => users.filter(user => {
          return user.username === newUser.username
        }))
      )
      .pipe(
        flatMap(users => {
          if (users.length > 0) {
            return throwError("Tài khoản đã có người sử dụng")
          }
          else {
            this.store.dispatch(
              new AddUser(newUser)
            )
            return EMPTY
          }
        })
      )
  }

  fetchIsAuth(): Observable<boolean>{
    return this.store.pipe(
      select(fetchCurrentUser)
    ).pipe(
      map(user => user != null)
    )
  }

  login(username: string, password: string): Observable<never> {
    return this.store.pipe(
      select(fetchUsers)
    )
      .pipe(
        first()
      )
      .pipe(
      map(users => users.find(user => user.username === username && user.password === password))
    ).pipe(
      flatMap(user => {
        if (user == null){
          return throwError("Tài khoản hoặc mật khẩu không chính xác")
        }
        else {
          this.store.dispatch(
            new SetCurrentUser(user)
          )
          return EMPTY
        }
      })
    )
  }

  fetchCurrentUser(): Observable<User> {
    return this.store.pipe(
      select(fetchCurrentUser)
    )
  }
  currentUserIsAdmin(): Observable<boolean>{
    return this.fetchCurrentUser().pipe(
      map(user => user != null && user.role === UserRole.Admin)
    )
  }

  fetchCurrentUserInformation(): Observable<{
    isAdmin: boolean,
    isAuth: boolean,
    fullName: string,
    username: string
  }> {
    return this.fetchCurrentUser()
      .pipe(
        map(user => {
          return {
            isAuth: user != null,
            isAdmin: user != null && user.role === UserRole.Admin,
            fullName: user != null ? user.fullName : "Khách",
            username: user != null ? user.username : null
          }
        })
      )
  }

  fetchUsers(): Observable<User[]> {
    return this.store.pipe(
      select(fetchUsers)
    )
  }

  updateUser(user: User): Observable<never> {
    this.store.dispatch(
      new UpdateUser(
        user
      )
    )
    return EMPTY
  }

  fetchCurrentUserRoleEditable(user: User): Observable<boolean> {
    return this.fetchCurrentUser().pipe(
      map(currentUser => {
        return user.role === UserRole.Member || user.adderId === currentUser.username
      })
    )
  }

  logout(): Observable<never> {
    this.store.dispatch(
      new SetCurrentUser(null)
    )
    return EMPTY
  }

  createUser(params: { birthday: string; password: string; thumbnail: string; phone: string; fullName: string; email: string; username: string }): Observable<never> {
    return this.fetchCurrentUser().pipe(
      first()
    )
      .pipe(
        flatMap(currentUser => {
          return this.register({...params, adderId: currentUser.username})
        })
      )
  }

  fetchUserProfile(userId: string): Observable<UserProfile> {
    return this.fetchUsers().pipe(
      map(users => users.find(user => user.username === userId))
    )
      .pipe(
        map(user => {
          return {
            username: user.username,
            fullName: user.fullName,
            birthday: user.birthday,
            email: user.email,
            phone: user.phone,
            role: user.role,
            thumbnail: user.thumbnail,
            adderId: user.adderId,
            created: user.created
          }
        })
      )
  }
  private fetchUser(userId: string): Observable<User>{
    return this.fetchUsers()
      .pipe(
        map(users => users.find(user => user.username === userId))
      )
  }
  updateUserProfile(profile: UserProfile): Observable<never> {
    return this.fetchUser(profile.username).pipe(
      first()
    )
      .pipe(
        flatMap(user => {
          user.fullName = profile.fullName
          user.email = profile.email
          user.phone = profile.phone
          user.thumbnail = profile.thumbnail
          user.birthday = profile.birthday
          this.store.dispatch(
            new UpdateUser(user)
          )
          return EMPTY
        })
      )
  }

  fetchCurrentUserProfileEditable(userId: string): Observable<boolean> {
    return this.fetchCurrentUser()
      .pipe(
        map(currentUser => {
          return currentUser.username === userId
        })
      )
  }

  changePassword(oldPassword: string, newPassword: string): Observable<never> {
    return this.fetchCurrentUser().pipe(
      first()
    ).pipe(
      flatMap(user => {
        if (user.password !== oldPassword){
          return throwError("Mật khẩu cũ không đúng")
        }
        else {
          user.password = newPassword
          this.store.dispatch(
            new UpdateUser(user)
          )
          return EMPTY
        }
      })
    )
  }
}
