import {range} from "rxjs"


export interface News{
  id: string,
  thumbnail: string,
  title: string,
  description: string,
  content: string,
  categoryId: string,
  categoryName: string,
  author: string,
  creatorId: string,
  created: number,
  images: ImageDescription[]
}
export interface Category{
  id: string,
  name: string,
  created: number
}

export interface CategoryNewses{
  category: Category,
  newses: News[]
}

export interface ImageDescription{
  src: string,
  description: string
}

export interface NewsComment{
  id: string,
  content: string,
  created: number,
  commenterId: string,
  commenterName: string,
  newsId: string,
  commenterThumbnail: string
}

export function createNewsComment(content: string, newsId: string, commenterId: string, commenterName: string, thumbnail: string): NewsComment{
  const now = Date.now()
  return {
    id: now.toString(),
    content: content,
    created: now,
    commenterId: commenterId,
    commenterName: commenterName,
    newsId: newsId,
    commenterThumbnail: thumbnail
  }
}

export function fakeCategories(): Category[]{
  const result = []
  for (let i = 1; i < 7; i++){
    const category = fakeCategory()
    category.id = i.toString()
    category.name = "Danh mục " + i
    result.push(category)
  }
  return result
}

export function fakeNewses(_categories: Category[]): News[] {
  const _newses = []
  _categories.forEach(category => {
    const firstNews = fakeNewsWithCategory(category)
    firstNews.id = "1" + category.id
    _newses.push(
      firstNews,
      fakeNewsWithCategory(category),
      fakeNewsWithCategory(category),
      fakeNewsWithCategory(category),
      fakeNewsWithCategory(category),
      fakeNewsWithCategory(category),
      fakeNewsWithCategory(category),
      fakeNewsWithCategory(category),
      fakeNewsWithCategory(category),
      fakeNewsWithCategory(category),
      fakeNewsWithCategory(category),
      fakeNewsWithCategory(category),
      fakeNewsWithCategory(category),
      fakeNewsWithCategory(category),
      fakeNewsWithCategory(category)
    )
  })
  return _newses
}

export function fakeCategory(): Category{
  const now = Date.now()
  const id = now.toString() + Math.random()
  return {
    id: id,
    name: "Danh mục " + id.substr(id.length - 2, 1),
    created: now
  }
}

export function fakeNewsWithCategory(category: Category): News{
  const news = fakeNews()
  news.categoryId = category.id
  news.categoryName = category.name
  return news
}

export function fakeNews(): News{
  const now = Date.now()
  return {
    id: now.toString()  + Math.random().toString(4),
    thumbnail: "https://www.dkn.tv/wp-content/uploads/2018/12/228.jpg",
    title: "Mừng sinh nhật lần thứ 200 của bản Thánh ca bất hủ được yêu thích nhất mùa Giáng Sinh",
    description: "Năm 2011, bản Thánh ca này được UNESCO công nhận là kiệt tác truyền khẩu và di sản văn hóa phi vật thể của nhân loại. Năm 2013, tạp chí Time sau một cuộc khảo sát đã tuyên bố đây là ca khúc Giáng sinh được yêu thích nhất trên thế giới với 733 lần được thu âm, tính riêng từ 1978 đến thời điểm đó.",
    content: "Giáng Sinh ngày nay đã là ngày lễ không chỉ của riêng người Công Giáo mà là của cả nhân loại. Điều gì làm nên một đêm Giáng Sinh đầy ý nghĩa? Chúng ta nghĩ tới Ông già Tuyết, quà Giáng Sinh, ánh sáng lung linh, đêm đông lạnh, tuyết trắng, món ngỗng quay, cây thông Noel, không khí ấm cúng trong các gia đình Ki Tô hữu hay hình ảnh náo nức ngoài phố của người ngoại đạo, v.v. nhưng nhất thiết không thể thiếu nhà Thờ Công Giáo và các bản Thánh Ca.\n" +
      "\n" +
      "Có một bản Thánh ca đặc biệt năm nay tròn 200 tuổi đã thể hiện sức sống trường tồn của nó bất chấp thời gian và những đổi thay của loài người. Bản Thánh ca này có lời ca được dịch ra ít nhất 140 thứ tiếng, chưa kể các loại thổ ngữ khác. Nó được thể hiện bởi vô số các giọng ca lẫy lừng, các dàn đồng ca danh tiếng trên thế giới. Không chỉ trong hoàn cảnh thanh bình, nó còn được hát ở hai bờ chiến tuyến và là lý do ngừng bắn của binh sĩ hai bên Anh – Đức trong thế chiến 2. Năm 2011, bản Thánh ca này được UNESCO công nhận là kiệt tác truyền khẩu và di sản văn hóa phi vật thể của nhân loại. Năm 2013, tạp chí Time sau một cuộc khảo sát đã tuyên bố đây là ca khúc Giáng sinh được yêu thích nhất trên thế giới với 733 lần được thu âm, tính riêng từ 1978 đến thời điểm đó. ",
    categoryId: now.toString(),
    author: "Bình Nguyên",
    creatorId: now.toString(),
    created: now,
    categoryName: now.toString(),
    images: [
      {
        src: "https://www.dkn.tv/wp-content/uploads/2018/12/maxresdefault-6.jpg",
        description: "Qua 200 năm, bản Thánh ca Silent Night đã thể hiện sức sống trường tồn của nó bất chấp thời gian và những đổi thay của loài người. (Ảnh từ youtube).\nChúng ta đang nói đến bản Silent Night huyền thoại.\n" +
          "\n" +
          "Hoàn cảnh ra đời của bài hát\n" +
          "\n" +
          "Ngày 24/12/1818, tại làng Oberndorf gần Salzburg nước Áo, có một vị cố đạo trẻ 25 tuổi tên là Joseph Mohr đang loay hoay chuẩn bị cho phần âm nhạc của buổi đêm Giáng Sinh. Ông phát hiện cây đàn Organ trong nhà thờ bị chuột gặm hỏng, do vậy dàn đồng ca sẽ không thể trình diễn được như thường lệ. Trong hoàn cảnh này chỉ có thể sử dụng tiết mục đơn ca và đệm bằng đàn guitar mà thôi. Nhưng với nhạc phẩm nào đây? Joseph Mohr chợt nhớ đến một bài thơ ông làm hai năm trước ngợi ca cảnh Chúa Jesus giáng sinh. Có lẽ phải phổ nhạc cho bài thơ này để sử dụng cho buổi lễ tối hôm đó. Ông nhớ đến người bạn của mình là Franz Gruber, một thầy giáo dạy nhạc ở trường làng và đem bài thơ đến nhờ Gruber phổ nhạc. Gruber nhận lời và rất nhanh chóng phổ nhạc cho bài thơ để hai người có thể bắt tay vào tập luyện.\n" +
          "\n" +
          "Và buổi đêm Giáng Sinh đó, dân làng Oberndorf hết sức ngạc nhiên và thích thú với tiết mục biểu diễn của hai nhạc công không chuyên. Một bài hát lạ lẫm được trình bày bởi giọng bass của Gruber và giọng tenor của Mohr, được đệm bằng đàn guitar… đã thành công rực rỡ và mang lại một không khí hết sức khác lạ, hết sức thiêng liêng.\n" +
          "\n" +
          "Từ đó, bản nhạc bắt đầu được Gruber lan truyền quanh khu vực Oberndorf và ngày càng đi xa hơn. Đặc biệt, ban nhạc Rainer Family đã trình diễn nó ở cung điện của Sa Hoàng Nga và Hoàng đế nước Áo vào năm 1822. Năm 1832, Silent Night được trình bày trước công chúng ở Leipzig. Vua Phổ Friedrich Wilhelm năm nào cũng mời dàn đồng ca nhà thờ tới lâu đài trình bày ca khúc này. Năm 1839, nó được chơi ở NewYork và rồi đi khắp thế giới theo chân các nhà truyền giáo. Chỉ có điều, người ta đã quên bẵng hai người cha đẻ của nó là Mohr và Gruber ở một ngôi làng bình dị nước Áo. Ca khúc hay được hiểu nhầm là dân ca Tylorean (vùng đất trong khu vực núi Alps ở Châu Âu) hoặc thậm chí được cho là tác phẩm của những nhà soạn nhạc lừng danh như Haydn, Mozart hay Beethoven. Mãi cho đến năm 1995, người ta mới tìm thấy bản chép tay của bản nhạc và khẳng định được chủ nhân đích thực của nó là Mohr và Gruber sau gần 200 năm bị rơi vào quên lãng."
      },
      {
        src: "https://www.dkn.tv/wp-content/uploads/2018/12/stille-mohr-gruber.jpg",
        description: "Hình vẽ Josef Mohr và Nhạc sĩ Franz Gruber đều là người Áo, đồng tác giả của bài Silent Night. (Ảnh: dkn).\nTại Việt Nam, bản Silent Night được chuyển lời Việt thành bản “Đêm Thánh vô cùng” khoảng hơn nửa thế kỷ trước bởi nhạc sĩ Hùng Lân.\n" +
          "\n" +
          "Nội dung của bản Silent Night – Đêm yên lặng\n" +
          "\n" +
          "Silent Night mô tả sự việc vào đêm huyền thoại khi Chúa Jesus giáng sinh với phần ca từ như sau (bản gốc tiếng Áo gồm 6 khổ thơ, bản tiếng Anh sau đây rút gọn còn 3 khổ thơ)\n" +
          "\n" +
          "Silent night, holy night!\n" +
          "\n" +
          "All is calm, all is bright.\n" +
          "\n" +
          "Round yon Virgin Mother and Child.\n" +
          "\n" +
          "Holy infant so tender and mild,\n" +
          "\n" +
          "Sleep in heavenly peace,\n" +
          "\n" +
          "Sleep in heavenly peace\n" +
          "\n" +
          " \n" +
          "\n" +
          "Silent night, holy night!\n" +
          "\n" +
          "Shepherds quake at the sight.\n" +
          "\n" +
          "Glories stream from heaven afar\n" +
          "\n" +
          "Heavenly hosts sing Alleluia,\n" +
          "\n" +
          "Christ the Savior is born!\n" +
          "\n" +
          "Christ the Savior is born\n" +
          "\n" +
          " \n" +
          "\n" +
          "Silent night, holy night!\n" +
          "\n" +
          "Son of God, love’s pure light.\n" +
          "\n" +
          "Radiant beams from Thy holy face\n" +
          "\n" +
          "With dawn of redeeming grace,\n" +
          "\n" +
          "Jesus Lord, at Thy birth\n" +
          "\n" +
          "Jesus Lord, at Thy birth.\n" +
          "\n" +
          " \n" +
          "\n" +
          "Tạm dịch:\n" +
          "\n" +
          "Đêm tĩnh mịch, đêm thiêng liêng!\n" +
          "\n" +
          "Mọi vật thật bình yên và rạng rỡ\n" +
          "\n" +
          "Xung quanh Đức Mẹ đồng trinh và con của bà\n" +
          "\n" +
          "Đức Chúa hài đồng thật hiền hậu và bé bỏng\n" +
          "\n" +
          "Yên ngủ trong cảnh thái bình thiên giới\n" +
          "\n" +
          "Yên ngủ trong cảnh thái bình thiên giới\n" +
          "\n" +
          " \n" +
          "\n" +
          "Đêm tĩnh mịch, đêm thiêng liêng!\n" +
          "\n" +
          "Những mục tử chấn động trước thần tích triển hiện\n" +
          "\n" +
          "Những luồng sáng mỹ diệu từ thiên đường phía xa\n" +
          "\n" +
          "Những thiên sứ hát vang bài “ngợi ca Thiên Chúa”\n" +
          "\n" +
          "Đấng Ki Tô Cứu Thế đã ra đời\n" +
          "\n" +
          "Đấng Ki Tô Cứu Thế đã ra đời\n" +
          "\n" +
          " \n" +
          "\n" +
          "Đêm tĩnh mịch, đêm thiêng liêng!\n" +
          "\n" +
          "Người con của Thiên Chúa, ánh sáng thánh khiết của tình yêu thương\n" +
          "\n" +
          "Những tia sáng lộng lẫy từ dung nhan của Thiên Chúa\n" +
          "\n" +
          "Với bình minh của ơn cứu rỗi\n" +
          "\n" +
          "Chúa Jesus giáng thế\n" +
          "\n" +
          "Chúa Jesus giáng thế."
      }
    ]
  }
}


export function createEmptyImageDescription(): ImageDescription{
  return {
    src: "https://i.imgur.com/Hovwcjy.png",
    description: "Miêu tả cho hình này đặt tại đây. Bấm vào icon chỉnh sửa để tiến hành chỉnh sửa"
  }
}

export function createEmptyNews(creatorId: string): News{
  const now = Date.now()
  return {
    id: now.toString(),
    thumbnail: "https://i.imgur.com/Hovwcjy.png",
    images: [
      createEmptyImageDescription()
    ],
    description: "Tóm tắt nội dung bài viết tại đây",
    author: "Tác giả bài viết",
    title: "Tiêu đề bài viết",
    categoryId: now.toString(),
    categoryName: "Thể loại của bài viết",
    created: now,
    creatorId: creatorId,
    content: "Nội dung của bài viết"
  }
}
