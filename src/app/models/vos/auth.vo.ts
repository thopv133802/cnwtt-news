export interface User{
  username: string
  password: string
  fullName: string
  birthday: string
  email: string
  phone: string
  role: UserRole,
  thumbnail: string,
  adderId: string,
  created: number
}

export interface UserProfile{
  username: string
  fullName: string
  birthday: string
  email: string
  phone: string
  role: UserRole,
  thumbnail: string,
  adderId: string,
  created: number
}

export enum UserRole{
  Admin = "Admin",
  Member = "Member"
}

export function parseUserRole(value: string): UserRole{
  if (value === "Admin"){
    return UserRole.Admin
  }
  else if (value === "Member") {
    return UserRole.Member
  }
}

export const UserRoles = [
  UserRole.Admin,
  UserRole.Member
]


export function createNewUser(params: { birthday: string; password: string; phone: string; fullName: string; email: string; username: string; thumbnail: string }): User{
  return {
    username: params.username,
    password: params.password,
    fullName: params.fullName,
    birthday: params.birthday,
    email: params.email,
    phone: params.phone,
    role: UserRole.Member,
    created: Date.now(),
    adderId: null,
    thumbnail: params.thumbnail == null ? "https://scontent.fhan5-2.fna.fbcdn.net/v/t1.0-9/48046764_2109560242472108_949191809685258240_n.jpg?_nc_cat=102&_nc_ht=scontent.fhan5-2.fna&oh=24cdef63be1c2608e2da8fef4b0de4e1&oe=5C8C02FD" : params.thumbnail
  }
}

export function fakeAdminUser(username: string, password: string): User{
  return {
    username: username,
    password: password,
    fullName: username,
    birthday: "02/12/1995",
    email: "jlaotsezu@gmail.com",
    phone: "0965784238",
    role: UserRole.Admin,
    thumbnail: "https://scontent.fhan5-2.fna.fbcdn.net/v/t1.0-9/48046764_2109560242472108_949191809685258240_n.jpg?_nc_cat=102&_nc_ht=scontent.fhan5-2.fna&oh=24cdef63be1c2608e2da8fef4b0de4e1&oe=5C8C02FD",
    adderId: null,
    created: Date.now()
  }
}
export function fakeMemberUser(username: string, password: string): User{
  return {
    username: username,
    password: password,
    fullName: username,
    birthday: "02/12/1995",
    email: "thopvna@gmail.com",
    phone: "0965784238",
    role: UserRole.Member,
    thumbnail: "https://scontent.fhan5-2.fna.fbcdn.net/v/t1.0-9/48046764_2109560242472108_949191809685258240_n.jpg?_nc_cat=102&_nc_ht=scontent.fhan5-2.fna&oh=24cdef63be1c2608e2da8fef4b0de4e1&oe=5C8C02FD",
    adderId: "jlaotsezu",
    created: Date.now()
  }
}
