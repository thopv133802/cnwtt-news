

export interface Post{
  id: string,
  title: string,
  created: number,
  content: string[],
  creatorName: string,
  creatorId: string,
  views: number
}
export interface PostComment{
  id: string,
  content: string,
  creatorId: string,
  creatorName: string,
  postId: string,
  created: number,
  creatorThumbnail: string
}
export function createNewPost(title: string, content: string[], creatorId: string, creatorName: string): Post{
  const now = Date.now()
  return {
    id: now.toString() + Math.random().toString(4),
    title: title,
    content: content,
    created: now,
    creatorName: creatorName,
    creatorId: creatorId,
    views: 0
  }
}
export function createNewPostComment(content: string, creatorId: string, creatorName: string, creatorThumbnail: string, postId: string): PostComment{
  const now = Date.now()
  return {
    id: now.toString() + Math.random().toString(4),
    content: content,
    creatorId: creatorId,
    postId: postId,
    created: now,
    creatorName: creatorName,
    creatorThumbnail: creatorThumbnail
  }
}
export function fakePost(): Post{
  return createNewPost("Bí quyết giúp mẹ bầu 3 tháng cuối ngủ ngon",
    [
      "Giai đoạn cuối thai kỳ, mẹ còn bao nhiêu nỗi lo như việc bà bầu nên uống sữa gì, nên chuẩn bị những gì để đón bé chào đời thật chu toàn. Thế mà chứng mất ngủ còn hành hạ mẹ mỗi đêm. Điều này khiến mẹ bầu vô cùng mệt mỏi vào ngày hôm sau, ảnh hưởng tới sự phát triển của thai nhi về lâu dài. Dưới đây sẽ là những thông tin tư vấn từ TS. BS Hoàng Thị Diễm Tuyết, Giám đốc Bệnh viện Hùng Vương giúp mẹ điều trị chứng mất ngủ vào giai đoạn cuối của thai kỳ.",
      "Nguyên nhân gây mất ngủ khi mang thai 3 tháng cuối",
      " - Vùng bụng to: Chỉ có thể nằm nghiêng một bên khiến mẹ có cảm giác bức bí. Những mẹ bầu hay trở mình sẽ càng khó ngủ hơn vì khó mà xoay qua xoay lại với phần bụng quá to.",
      " - Đi tiểu thường xuyên: Trong những tháng cuối do áp lực của tử cung chèn lên bàng quang dễ khiến mẹ bầu tỉnh dậy giữa đêm và khó có thể ngủ lại.",
      " - Tim đập nhanh: Khi thai nhi thích hoạt động về đêm hơn ngày, tim mẹ bầu sẽ đập nhanh hơn để bơm đủ lượng máu vào tử cung. Điều này khiến mẹ khó chìm vào giấc ngủ khi cơ thể chưa được thư giãn." +
      " - Khó thở: Do tử cung to lên chèn vào cơ hoành nằm dưới phổi gây ra tình trạng khó thở, làm mẹ bầu trằn trọc cả đêm.",
      " - Những cơn đau xuất hiện ở lưng hay chân bị chuột rút cũng thườngquấy rối mẹ bầu vào ban đêm. Ngoài ra còn có những cơn co thắt giả trong những tháng cuối thai kỳ.",
      "Điều trị chứng mất ngủ cho mẹ bầu",
      "Ngay từ khi mang thai, mẹ bầu nên tập nằm nghiêng về bên trái. Đây là tư thể ngủ tốt nhất dành cho mẹ bầu, không gây chèn ép lên bụng bầu đồng thời giúp lượng máu lưu thông từ mẹ vào thai nhi dễ dàng hơn. Nếu bị chuột rút giữa đêm, mẹ nên xoa bóp nhẹ nhàng hoặc chườm nóng. Hàng ngày mẹ nên tập những bài tập thể dục cho bà bầu ở 3 tháng cuối để loại bỏ chứng chuột rút cũng như hạn chế tình trạng đau lưng, vai, gáy.",
      "Chế độ dinh dưỡng:",
      " - Hạn chế uống nước vào buổi tối. Hãy chia đều lượng nước phải uống vào ban ngày. Trước khi đi ngủ nên tập thói quen đi tiểu để bàng quang đủ sức chứa lượng nước tiểu cả đêm.",
      " - Cần lưu ý để không bị đầy hơi, khó tiêu sau các bữa ăn. Hãy chia nhỏ bữa ăn thành 4 – 5 bữa/ngày và chỉ ăn ở mức độ vừa phải, không quá no. Tránh ăn thức ăn quá nhiều dầu mỡ, cay nóng vì chúng rất dễ gây đầy bụng.",
      "Tắm nước ấm hay uống một ly sữa ấm hoặc trà gừng để cơ thể thư giãn. Nhiều mẹ bầu lại có cảm giác an tâm khi ngủ bằng việc sử dụng gối ôm hoặc kê gối vào chân. Đó đều là những cách giúp cơ thể thư giãn, tâm trạng thoải mái, dễ dàng giúp đi vào giấc ngủ hơn.",
      "Khi khó ngủ, mẹ có thể đọc một cuốn sách, sẽ khiến mẹ nhanh mỏi mắt và dễ buồn ngủ hơn. Một chú ý nữa là khi ngủ mẹ nên để nhiệt độ phòng ở mức mát mẻ cũng như mặc quần áo rộng rãi để cơ thể thực sự được thoải mái."
    ],
    "jlaotsezu", "Laotsezu")
}
export function fakePosts(): Post[]{
  const aPost = fakePost()
  aPost.id = "1"
  return [
    aPost,
    fakePost(),
    fakePost(),
    fakePost(),
    fakePost(),
    fakePost(),
    fakePost()
  ]
}
