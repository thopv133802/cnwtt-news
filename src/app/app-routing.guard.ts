import {Injectable} from "@angular/core"
import {ActivatedRouteSnapshot, CanActivate, CanLoad, Route, Router, RouterStateSnapshot, UrlSegment} from "@angular/router"
import {AuthService} from "./models/services/auth.service"
import {Observable} from "rxjs"
import {first, map} from "rxjs/operators"
import {UserRole} from "./models/vos/auth.vo"


@Injectable()
export class IsAdminGuard implements CanActivate, CanLoad{
  constructor(private router: Router, private authService: AuthService){}
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.authService.fetchCurrentUser()
      .pipe(
        first()
      )
      .pipe(
        map(user => user.role === UserRole.Admin)
      )
  }

  canLoad(route: Route, segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
    return this.authService.fetchCurrentUser()
      .pipe(
        first()
      )
      .pipe(
        map(user => user.role === UserRole.Admin)
      )
  }
}

@Injectable()
export class IsAuthGuard implements  CanActivate{
  constructor(private router: Router, private authService: AuthService){}
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.authService.fetchIsAuth()
      .pipe(
        first()
      )
  }
}

@Injectable()
export class IsNotAuthGuard implements CanActivate{
  constructor(private router: Router, private authService: AuthService){}
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.authService.fetchIsAuth()
      .pipe(
        first()
      )
      .pipe(
        map(isAuth => !isAuth)
      )
  }
}


@Injectable()
export class ChangePasswordGuard implements CanActivate{
  constructor(private authService: AuthService){}
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.authService.fetchCurrentUser()
      .pipe(
        first()
      ).pipe(
        map(currentUser => {
          console.log("ID: " + route.paramMap.get("id"))
          return currentUser.username === route.paramMap.get("id")
        })
      )
  }
}
