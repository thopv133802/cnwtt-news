import { Component, OnInit } from '@angular/core';
import {News} from "../../models/vos/news.vo"
import {NewsService} from "../../models/services/news.service"
import {ActivatedRoute, Router} from "@angular/router"

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  firstNews: News
  verticalNewses: News[]
  horizontalNewses: News[]
  constructor(private newsService: NewsService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.newsService.fetchNewsesByCategory(params.get("id")).subscribe(newses => {
        this.firstNews = newses[0]
        this.horizontalNewses = newses.slice(1, 10)
        this.verticalNewses = newses.slice(11)
      })
    })
  }

  navigateToFirstNews() {
    this.router.navigate(["/news", this.firstNews.id])
  }
}
