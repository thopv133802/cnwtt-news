import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryVerticalNewsComponent } from './category-vertical-news.component';

describe('CategoryVerticalNewsComponent', () => {
  let component: CategoryVerticalNewsComponent;
  let fixture: ComponentFixture<CategoryVerticalNewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoryVerticalNewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryVerticalNewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
