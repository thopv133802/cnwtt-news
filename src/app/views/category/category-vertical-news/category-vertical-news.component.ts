import {Component, Input, OnInit} from "@angular/core"
import {News} from "../../../models/vos/news.vo"
import {Router} from "@angular/router"

@Component({
  selector: 'app-category-vertical-news',
  templateUrl: './category-vertical-news.component.html',
  styleUrls: ['./category-vertical-news.component.css']
})
export class CategoryVerticalNewsComponent implements OnInit {
  @Input()
  news: News
  constructor(private router: Router) { }

  ngOnInit() {
  }

  navigateToDetail() {
    this.router.navigate(["/news", this.news.id])
  }
}
