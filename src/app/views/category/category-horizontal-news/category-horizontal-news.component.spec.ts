import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryHorizontalNewsComponent } from './category-horizontal-news.component';

describe('CategoryHorizontalNewsComponent', () => {
  let component: CategoryHorizontalNewsComponent;
  let fixture: ComponentFixture<CategoryHorizontalNewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoryHorizontalNewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryHorizontalNewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
