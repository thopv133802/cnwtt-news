import {Component, Input, OnInit} from "@angular/core"
import {News} from "../../../models/vos/news.vo"
import {Router} from "@angular/router"

@Component({
  selector: 'app-category-horizontal-news',
  templateUrl: './category-horizontal-news.component.html',
  styleUrls: ['./category-horizontal-news.component.css']
})
export class CategoryHorizontalNewsComponent implements OnInit {

  @Input()
  news: News
  constructor(private router: Router) { }

  ngOnInit() {
  }

  navigateToDetail() {
    this.router.navigate(["/news", this.news.id])
  }
}
