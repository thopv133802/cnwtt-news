import { Component, OnInit } from '@angular/core';
import {UserProfile} from "../../../models/vos/auth.vo"
import {ActivatedRoute, Router} from "@angular/router"
import {AuthService} from "../../../models/services/auth.service"
import {UtilsService} from "../../utils.service"

@Component({
  selector: 'app-update-profile',
  templateUrl: './update-profile.component.html',
  styleUrls: ['./update-profile.component.css']
})
export class UpdateProfileComponent implements OnInit {

  profile: UserProfile
  username: string
  fullName: string
  email: string
  phone: string
  birthday: string
  thumbnail: string
  editable = false

  constructor(private route: ActivatedRoute, private authService: AuthService, private uiService: UtilsService, private router: Router) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.authService.fetchUserProfile(params.get("id")).subscribe(profile => {
        this.bindProfile(profile)
      })
      this.authService.fetchCurrentUserProfileEditable(params.get("id")).subscribe(editable => {
        this.editable = editable
      })
    })
  }

  private bindProfile(profile: UserProfile) {
    this.profile = profile
    console.log(profile)
    this.username = this.profile.username
    this.fullName = this.profile.fullName
    this.email = this.profile.email
    this.phone = this.profile.phone
    this.birthday = this.profile.birthday
    this.thumbnail = this.profile.thumbnail
  }

  navigateToChangePassword() {
    this.router.navigate(["profile/change-password/" + this.username])
  }

  requestUpdateProfile() {
    this.profile.fullName = this.fullName
    this.profile.email = this.email
    this.profile.phone = this.phone
    this.profile.birthday = this.birthday
    this.profile.thumbnail = this.thumbnail

    this.authService.updateUserProfile(this.profile).subscribe(null, error => {
      this.uiService.showSnackBar(error)
    }, () => {
      this.uiService.showSnackBar("Cập nhật hồ sơ cá nhân thành công")
    })
  }

}
