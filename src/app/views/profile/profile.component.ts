import { Component, OnInit } from '@angular/core';
import {User, UserProfile} from "../../models/vos/auth.vo"
import {ActivatedRoute} from "@angular/router"
import {AuthService} from "../../models/services/auth.service"
import {UtilsService} from "../utils.service"

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  ngOnInit(): void {
  }
}
