import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../../models/services/auth.service"
import {Router} from "@angular/router"
import {UtilsService} from "../../utils.service"

import {Location} from "@angular/common"

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  oldPassword: string
  newPassword: string
  confirmNewPassword: string
  confirmPasswordWrong = true

  constructor(private authService: AuthService, private router: Router, private uiService: UtilsService, private location: Location) { }

  ngOnInit() {

  }

  requestChangePassword() {
    this.authService.changePassword(this.oldPassword, this.newPassword).subscribe(null, error => {
      this.uiService.showSnackBar(error)
    }, () => {
      this.uiService.showSnackBar("Thay đổi mật khẩu thành công")
      this.location.back()
    })
  }

  newPasswordChanged() {
   this.confirmPasswordWrong = this.confirmNewPassword !== this.newPassword
  }
}
