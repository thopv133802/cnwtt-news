import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router"
import {AuthService} from "../../models/services/auth.service"
import {UtilsService} from "../utils.service"

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(private router: Router, private authService: AuthService, private utilService: UtilsService) { }

  ngOnInit() {
    this.authService.logout().subscribe(null, error => {
      this.utilService.showSnackBar(error)
    }, () => {
      this.router.navigate(["/home"])
    })

  }

}
