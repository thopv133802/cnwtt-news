import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router"
import {NewsService} from "../../../models/services/news.service"
import {News} from "../../../models/vos/news.vo"

@Component({
  selector: 'app-news-management',
  templateUrl: './news-management.component.html',
  styleUrls: ['./news-management.component.css']
})
export class NewsManagementComponent implements OnInit {
  liveKeyword: string
  keyword: string
  newses: News[]
  searchedNewses: News[]
  showResultLabel = false
  constructor(private route: ActivatedRoute, private newsService: NewsService, private router: Router) { }

  ngOnInit() {
    this.newsService.fetchNewses().subscribe(newses => {
      this.newses = newses
      this.requestSearch()
    })
  }


  requestSearch() {
    this.keyword = this.liveKeyword
    if (this.keyword == null || this.keyword.length === 0) {
      this.searchedNewses = this.newses
      this.showResultLabel = false
    }
    else {
      this.searchedNewses = this.newses.filter(news => {
        return news.title.includes(this.keyword)
          || news.author.includes(this.keyword)
          || news.content.includes(this.keyword)
      })
      this.showResultLabel = true
    }
  }

  requestAddNewNews() {
    this.router.navigate(["administrate/news-management/create"])
  }

  keywordChange() {
    if (this.liveKeyword == null || this.liveKeyword.length === 0){
      this.requestSearch()
    }
  }
}
