import { Component, OnInit } from '@angular/core';
import {createEmptyImageDescription, fakeNews, News} from "../../../../models/vos/news.vo"
import {NewsService} from "../../../../models/services/news.service"
import {UtilsService} from "../../../utils.service"
import {AuthService} from "../../../../models/services/auth.service"
import {first, flatMap, map} from "rxjs/operators"
import {throwError} from "rxjs"
import {Location} from "@angular/common"
@Component({
  selector: 'app-create-news',
  templateUrl: './create-news.component.html',
  styleUrls: ['./create-news.component.css']
})
export class CreateNewsComponent implements OnInit {
  news: News

  constructor(private newsService: NewsService, private utilsService: UtilsService, private location: Location) { }

  ngOnInit() {
    this.newsService.createEmptyNews()
      .subscribe(news => {
        this.news = news
      })
  }

  requestAddNews() {
    this.newsService.createNews(this.news).subscribe(null, error => {
      this.utilsService.showSnackBar("Thêm tin tức thất bại" + error)
    }, () => {
      this.utilsService.showSnackBar("Thêm tin tức thành công")
      this.location.back()
    })
  }

  requestAddNewImageNews() {
    this.news.images.push(
      createEmptyImageDescription()
    )
  }
}
