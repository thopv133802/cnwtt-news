import {Component, Input, OnInit} from "@angular/core"
import {News} from "../../../../models/vos/news.vo"
import {Router} from "@angular/router"

@Component({
  selector: 'app-news-management-news-card',
  templateUrl: './news-management-news-card.component.html',
  styleUrls: ['./news-management-news-card.component.css']
})
export class NewsManagementNewsCardComponent implements OnInit {
  @Input()
  news: News

  constructor(private router: Router) { }

  ngOnInit() {
  }

  navigateToEditNews() {
    this.router.navigate(["administrate/news-management/edit/" + this.news.id])
  }
}
