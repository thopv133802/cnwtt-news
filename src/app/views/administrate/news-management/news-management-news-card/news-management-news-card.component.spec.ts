import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsManagementNewsCardComponent } from './news-management-news-card.component';

describe('NewsManagementNewsCardComponent', () => {
  let component: NewsManagementNewsCardComponent;
  let fixture: ComponentFixture<NewsManagementNewsCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsManagementNewsCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsManagementNewsCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
