import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core"

@Component({
  selector: 'app-editable-image',
  templateUrl: './editable-image.component.html',
  styleUrls: ['./editable-image.component.css']
})
export class EditableImageComponent implements OnInit {
  @Input()
  title = "Thay đổi hình ảnh"
  @Input()
  src: string

  @Output()
  imageUpdateListener = new EventEmitter<string>()
  editing: boolean

  constructor() { }

  ngOnInit() {
  }

  onAcceptButtonClicked() {
    this.imageUpdateListener.next(this.src)
    this.editing = false
  }

  onCancelButtonClicked() {
    this.editing = false
  }
}
