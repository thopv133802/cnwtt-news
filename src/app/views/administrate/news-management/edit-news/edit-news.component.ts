import { Component, OnInit } from '@angular/core';
import {createEmptyImageDescription, News} from "../../../../models/vos/news.vo"
import {ActivatedRoute} from "@angular/router"
import {NewsService} from "../../../../models/services/news.service"
import {UtilsService} from "../../../utils.service"

@Component({
  selector: 'app-edit-news',
  templateUrl: './edit-news.component.html',
  styleUrls: ['./edit-news.component.css']
})
export class EditNewsComponent implements OnInit {
  news: News


  constructor(private route: ActivatedRoute, private newsService: NewsService, private uiService: UtilsService) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.newsService.fetchNews(params.get("id")).subscribe(news => {
        this.news = news
      })
    })
  }

  requestUpdateNews() {
    this.newsService.updateNews(this.news).subscribe(null, error => {
      this.uiService.showSnackBar(error)
    }, () => {
      this.uiService.showSnackBar("Cập nhật tin tức thành công")
    })
  }

  requestAddNewImageNews() {
    this.news.images.push(createEmptyImageDescription())
    this.requestUpdateNews()
  }
}
