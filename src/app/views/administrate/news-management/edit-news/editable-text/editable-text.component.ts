import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core"

@Component({
  selector: 'app-editable-text',
  templateUrl: './editable-text.component.html',
  styleUrls: ['./editable-text.component.css']
})
export class EditableTextComponent implements OnInit {
  @Input()
  content: string
  @Input()
  textStyleClass: string
  @Input()
  textStyle: string

  @Input()
  textAreaHeight: number

  @Input()
  enableRemovable: boolean

  liveContent: string

  title = "Chỉnh sửa nội dung"
  editing = false

  @Output()
  contentUpdateListener = new EventEmitter<string>()
  @Output()
  onRemoveListener = new EventEmitter()

  constructor() { }

  ngOnInit() {
    this.liveContent = this.content
    if (this.enableRemovable === undefined){
      this.enableRemovable = false
    }
  }

  onAcceptButtonClicked() {
    this.content = this.liveContent
    this.contentUpdateListener.next(this.content)
    this.editing = false
  }

  onCancelButtonClick() {
    this.editing = false
  }

  triggerRemoveEvent() {
    this.onRemoveListener.next()
  }
}
