import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserManagementCardComponent } from './user-management-card.component';

describe('UserManagementCardComponent', () => {
  let component: UserManagementCardComponent;
  let fixture: ComponentFixture<UserManagementCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserManagementCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserManagementCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
