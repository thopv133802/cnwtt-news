import {Component, Input, OnInit} from "@angular/core"
import {parseUserRole, User, UserRole, UserRoles} from "../../../../models/vos/auth.vo"
import {MatSelectChange} from "@angular/material"
import {AuthService} from "../../../../models/services/auth.service"
import {UtilsService} from "../../../utils.service"

@Component({
  selector: 'app-user-management-card',
  templateUrl: './user-management-card.component.html',
  styleUrls: ['./user-management-card.component.css']
})
export class UserManagementCardComponent implements OnInit {
  @Input()
  user: User
  newRole: UserRole
  userRoles = UserRoles

  changed = false
  roleEditable = false

  constructor(private authService: AuthService, private uiService: UtilsService) { }

  ngOnInit() {
    this.authService.fetchCurrentUserRoleEditable(this.user).subscribe(editable => {
     this.roleEditable = editable
    })
  }

  onRoleChange($event: MatSelectChange) {
    this.newRole = parseUserRole($event.value)
    this.changed = this.user.role !== this.newRole;
  }

  requestUpdateChange() {
    this.user.role = this.newRole
    this.authService.updateUser(this.user).subscribe(null, error => {
      this.uiService.showSnackBar(error)
    }, () => {
      this.uiService.showSnackBar("Cập nhật thành công")
      this.changed = false
    })
  }
}
