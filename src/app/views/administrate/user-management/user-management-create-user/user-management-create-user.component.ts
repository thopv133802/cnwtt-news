import { Component, OnInit } from '@angular/core';
import {NgForm} from "@angular/forms"
import {AuthService} from "../../../../models/services/auth.service"
import {UtilsService} from "../../../utils.service"
import {Location} from "@angular/common"
@Component({
  selector: 'app-user-management-create-user',
  templateUrl: './user-management-create-user.component.html',
  styleUrls: ['./user-management-create-user.component.css']
})
export class UserManagementCreateUserComponent implements OnInit {
  username: string
  password: string
  fullName: string
  birthday: string
  email: string
  phone: string
  thumbnail: string

  constructor(private authService: AuthService, private utilsService: UtilsService, private location: Location) { }

  ngOnInit() {
  }

  requestCreateUser(createUserForm: NgForm) {
    this.authService.createUser({
      username: this.username,
      password: this.password,
      fullName: this.fullName,
      birthday: this.birthday,
      email: this.email,
      phone: this.phone,
      thumbnail: this.thumbnail
    }).subscribe(null, error => {
      this.utilsService.showSnackBar("Thêm người dùng mới thất bại: " + error)
    }, () => {
      this.utilsService.showSnackBar("Thêm người dùng mới thành công")
      createUserForm.resetForm()
      this.location.back()
    })

  }
}
