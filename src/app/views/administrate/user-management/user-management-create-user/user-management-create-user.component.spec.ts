import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserManagementCreateUserComponent } from './user-management-create-user.component';

describe('UserManagementCreateUserComponent', () => {
  let component: UserManagementCreateUserComponent;
  let fixture: ComponentFixture<UserManagementCreateUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserManagementCreateUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserManagementCreateUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
