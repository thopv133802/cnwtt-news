import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../../models/services/auth.service"
import {User} from "../../../models/vos/auth.vo"
import {Router} from "@angular/router"

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.css']
})
export class UserManagementComponent implements OnInit {
  liveKeyword: string
  keyword: string
  users: User[]
  searchedUsers: User[]
  showResultLabel = false



  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.authService.fetchUsers().subscribe(users => {
      this.users = users
      this.requestSearch()
    })
  }

  requestSearch() {
    this.keyword = this.liveKeyword
    if (this.keyword == null || this.keyword.length === 0) {
      this.searchedUsers = this.users
      this.showResultLabel = false
    }
    else {
      this.searchedUsers = this.users.filter(user => {
        return user.username.includes(this.keyword)
          || user.fullName.includes(this.keyword)
          || user.phone.includes(this.keyword)
          || user.email.includes(this.keyword)
      })
      this.showResultLabel = true
    }
  }

  keywordChange() {
    if (this.liveKeyword == null || this.liveKeyword === ""){
      this.requestSearch()
    }
  }

  requestAddNewUser() {
    this.router.navigate(["administrate/user-management/create"])
  }
}
