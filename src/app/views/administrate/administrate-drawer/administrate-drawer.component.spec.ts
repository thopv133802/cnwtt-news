import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministrateDrawerComponent } from './administrate-drawer.component';

describe('AdministrateDrawerComponent', () => {
  let component: AdministrateDrawerComponent;
  let fixture: ComponentFixture<AdministrateDrawerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdministrateDrawerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministrateDrawerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
