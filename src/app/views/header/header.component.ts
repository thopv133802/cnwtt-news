import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core"
import {NewsService} from "../../models/services/news.service"
import {Category} from "../../models/vos/news.vo"
import {Router} from "@angular/router"

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  categories: Category[]

  @Output()
  menuIconClickListener = new EventEmitter()
  @Input()
  title: string

  constructor() { }

  ngOnInit() {
    if (this.title === undefined){
      this.title = "Fournews"
    }
  }

  onMenuButtonClicked() {
    this.menuIconClickListener.next()
  }
}
