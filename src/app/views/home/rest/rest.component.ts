import { Component, OnInit } from '@angular/core';
import {CategoryNewses, News} from "../../../models/vos/news.vo"
import {NewsService} from "../../../models/services/news.service"
import {skip} from "rxjs/operators"

@Component({
  selector: 'app-rest',
  templateUrl: './rest.component.html',
  styleUrls: ['./rest.component.css']
})
export class RestComponent implements OnInit {
  newses: News[]
  categoriesNewses: CategoryNewses[]

  constructor(private newsService: NewsService) { }

  ngOnInit() {
    this.newsService.fetchNewses()
      .subscribe(newses => {
      this.newses = newses.slice(11, 20)
    })
    this.newsService.fetchCategoriesNewses().subscribe(categoriesNewses =>{
      this.categoriesNewses = categoriesNewses
    })
  }
}
