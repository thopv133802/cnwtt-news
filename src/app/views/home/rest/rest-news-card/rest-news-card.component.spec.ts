import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestNewsCardComponent } from './rest-news-card.component';

describe('RestNewsCardComponent', () => {
  let component: RestNewsCardComponent;
  let fixture: ComponentFixture<RestNewsCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestNewsCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestNewsCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
