import {Component, Input, OnInit} from "@angular/core"
import {News} from "../../../../models/vos/news.vo"
import {Router} from "@angular/router"

@Component({
  selector: 'app-rest-news-card',
  templateUrl: './rest-news-card.component.html',
  styleUrls: ['./rest-news-card.component.css']
})
export class RestNewsCardComponent implements OnInit {
  @Input()
  news: News

  constructor(private router: Router) { }

  ngOnInit() {
  }

  navigateToDetail() {
    this.router.navigate(["/news/", this.news.id])
  }
}
