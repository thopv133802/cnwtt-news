import {Component, Input, OnInit} from "@angular/core"
import {Category, CategoryNewses, News} from "../../../../models/vos/news.vo"
import {Router} from "@angular/router"

@Component({
  selector: 'app-rest-cat',
  templateUrl: './rest-cat.component.html',
  styleUrls: ['./rest-cat.component.css']
})
export class RestCatComponent implements OnInit {
  @Input()
  categoryNewses: CategoryNewses
  category: Category
  firstNews: News
  restNewses: News[]
  constructor(private router: Router) { }

  ngOnInit() {
    this.category = this.categoryNewses.category
    this.firstNews = this.categoryNewses.newses[0]
    this.restNewses = this.categoryNewses.newses.slice(1, 3)
  }

  navigateToNews(news: News) {
    this.router.navigate(["/news/", news.id])
  }
}
