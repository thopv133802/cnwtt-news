import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestCatComponent } from './rest-cat.component';

describe('RestCatComponent', () => {
  let component: RestCatComponent;
  let fixture: ComponentFixture<RestCatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestCatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestCatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
