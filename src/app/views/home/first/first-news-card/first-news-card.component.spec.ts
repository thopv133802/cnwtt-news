import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirstNewsCardComponent } from './first-news-card.component';

describe('FirstNewsCardComponent', () => {
  let component: FirstNewsCardComponent;
  let fixture: ComponentFixture<FirstNewsCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirstNewsCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirstNewsCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
