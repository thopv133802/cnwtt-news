import {Component, Input, OnInit} from "@angular/core"
import {News} from "../../../../models/vos/news.vo"
import {Router} from "@angular/router"

@Component({
  selector: 'app-first-news-card',
  templateUrl: './first-news-card.component.html',
  styleUrls: ['./first-news-card.component.css']
})
export class FirstNewsCardComponent implements OnInit {
  @Input()
  news: News
  constructor(private router: Router) { }

  ngOnInit() {
  }

  navigateToDetail() {
    this.router.navigate(["/news/", this.news.id])
  }
}
