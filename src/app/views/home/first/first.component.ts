import { Component, OnInit } from '@angular/core';
import {News} from "../../../models/vos/news.vo"
import {NewsService} from "../../../models/services/news.service"
import {Router} from "@angular/router"

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.css']
})
export class FirstComponent implements OnInit {
  firstNews: News
  newses: News[]

  constructor(private newsService: NewsService, private router: Router) { }

  ngOnInit() {
    this.newsService.fetchNewses().subscribe(newses => {
      this.firstNews = newses[0]
      this.newses = newses.slice(1, 10)
    })
  }

  navigateToFirstNews() {
    this.router.navigate(["/news/", this.firstNews.id])
  }
}
