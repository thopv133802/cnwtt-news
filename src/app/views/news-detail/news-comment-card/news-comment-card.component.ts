import {Component, Input, OnInit} from "@angular/core"
import {NewsComment} from "../../../models/vos/news.vo"

@Component({
  selector: 'app-news-comment-card',
  templateUrl: './news-comment-card.component.html',
  styleUrls: ['./news-comment-card.component.css']
})
export class NewsCommentCardComponent implements OnInit {
  @Input()
  comment: NewsComment
  constructor() { }

  ngOnInit() {

  }

}
