import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsCommentCardComponent } from './news-comment-card.component';

describe('NewsCommentCardComponent', () => {
  let component: NewsCommentCardComponent;
  let fixture: ComponentFixture<NewsCommentCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsCommentCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsCommentCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
