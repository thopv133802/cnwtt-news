import {Component, Input, OnInit} from "@angular/core"
import {News, NewsComment} from "../../../models/vos/news.vo"
import {NewsService} from "../../../models/services/news.service"
import {AuthService} from "../../../models/services/auth.service"
import {Router} from "@angular/router"
import {UtilsService} from "../../utils.service"
import {NgForm} from "@angular/forms"

@Component({
  selector: 'app-news-comments',
  templateUrl: './news-comments.component.html',
  styleUrls: ['./news-comments.component.css']
})
export class NewsCommentsComponent implements OnInit {
  @Input()
  news: News
  comments: NewsComment[]
  newComment: string
  isAuth = false
  constructor(private newsService: NewsService, private authService: AuthService, private router: Router, private utilsService: UtilsService) { }

  ngOnInit() {
    this.newsService.fetchComments(this.news.id).subscribe(comments => {
      this.comments = comments
    })
    this.authService.fetchIsAuth().subscribe(isAuth  => {
      this.isAuth = isAuth
    })
  }

  navigateToLogin() {
    this.router.navigate(["/login"])
  }

  addComment(form: NgForm) {
    this.newsService.addComment(this.newComment, this.news.id).subscribe(null, error => {
        this.utilsService.showSnackBar(error)
    }, () => {
      this.utilsService.showSnackBar("Thêm bình luận thành công")
      form.resetForm()
    })
  }
}
