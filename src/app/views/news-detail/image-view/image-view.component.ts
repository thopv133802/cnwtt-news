import {Component, Input, OnInit} from "@angular/core"
import {ImageDescription} from "../../../models/vos/news.vo"

@Component({
  selector: 'app-image-view',
  templateUrl: './image-view.component.html',
  styleUrls: ['./image-view.component.css']
})
export class ImageViewComponent implements OnInit {
  @Input()
  image: ImageDescription

  constructor() { }

  ngOnInit() {
  }

}
