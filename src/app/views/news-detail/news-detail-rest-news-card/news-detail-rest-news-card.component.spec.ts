import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsDetailRestNewsCardComponent } from './news-detail-rest-news-card.component';

describe('NewsDetailRestNewsCardComponent', () => {
  let component: NewsDetailRestNewsCardComponent;
  let fixture: ComponentFixture<NewsDetailRestNewsCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsDetailRestNewsCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsDetailRestNewsCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
