import {Component, Input, OnInit} from "@angular/core"
import {News} from "../../../models/vos/news.vo"
import {Router} from "@angular/router"
import {Location} from "@angular/common"
@Component({
  selector: 'app-news-detail-rest-news-card',
  templateUrl: './news-detail-rest-news-card.component.html',
  styleUrls: ['./news-detail-rest-news-card.component.css']
})
export class NewsDetailRestNewsCardComponent implements OnInit {
  @Input()
  news: News

  constructor(private router: Router, private location: Location) { }

  ngOnInit() {

  }

  navigateToDetail() {
    this.router.navigate(["/news/" + this.news.id])
  }
}
