import {Component, OnDestroy, OnInit} from "@angular/core"
import {ActivatedRoute} from "@angular/router"
import {News} from "../../models/vos/news.vo"
import {NewsService} from "../../models/services/news.service"
import {flatMap, map} from "rxjs/operators"
import {Subscription} from "rxjs"

@Component({
  selector: 'app-news-detail',
  templateUrl: './news-detail.component.html',
  styleUrls: ['./news-detail.component.css']
})
export class NewsDetailComponent implements OnInit, OnDestroy {
  news: News
  relatedNews: News[]

  constructor(private newsService: NewsService, private route: ActivatedRoute) { }

  subscriptions: Subscription[] = []

  ngOnInit() {
    const routeParamSubscription =  this.route.paramMap.subscribe(paramMap => {
      const newsId = paramMap.get("id")
      this.newsService.fetchNews(newsId).subscribe(news => {
        this.news = news
      })
      this.newsService.fetchNews(newsId)
        .pipe(
          flatMap(news => {
            return this.newsService.fetchNewsesByCategory(news.categoryId)
          })
        )
        .subscribe(newses => {
          this.relatedNews = newses.filter(news => news.id !== newsId)
        })
    })
    this.subscriptions.push(routeParamSubscription)
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe()
    })
  }
}
