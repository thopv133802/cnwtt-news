import { Component, OnInit } from '@angular/core';
import {NgForm} from "@angular/forms"
import {AuthService} from "../../models/services/auth.service"
import {Location} from "@angular/common"
import {UtilsService} from "../utils.service"
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username = ""
  password = ""
  constructor(private location: Location, private authService: AuthService, private utilsService: UtilsService) { }

  ngOnInit() {
  }

  requestLogin(loginForm: NgForm) {
    this.authService.login(this.username, this.password).subscribe( null, error => {
      this.utilsService.showSnackBar(error)
    }, () => {
      this.utilsService.showSnackBar("Đăng nhập thành công")
      loginForm.resetForm()
      this.location.back()
    })
  }
}
