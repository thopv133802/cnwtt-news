import { Component, OnInit } from '@angular/core';
import {NgForm} from "@angular/forms"
import {AuthService} from "../../models/services/auth.service"
import {UtilsService} from "../utils.service"
import {Router} from "@angular/router"
import {Location} from "@angular/common"
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  username = ""
  password = ""
  fullName = ""
  birthday = ""
  email = ""
  phone = ""
  thumbnail = ""

  constructor(private authService: AuthService, private utilsService: UtilsService, private router: Router, private location: Location) { }

  ngOnInit() {
  }

  requestRegister(loginForm: NgForm) {
    this.authService.register({
      username: this.username,
      password: this.password,
      fullName: this.fullName,
      birthday: this.birthday,
      email: this.email,
      phone: this.phone,
      thumbnail: this.thumbnail,
      adderId: null
    }).subscribe(null, error => {
      this.utilsService.showSnackBar("Đăng ký người dùng thất bại: " + error)
    }, () => {
      this.utilsService.showSnackBarWithAction("Đăng ký người dùng thành công", "Đăng nhập").subscribe(() => {
        this.router.navigate(["/home"]).then(value => {
          this.router.navigate(["/login"])
        })

      })
      loginForm.resetForm()
    })
  }
}
