import { Component, OnInit } from '@angular/core';
import {Category} from "../../models/vos/news.vo"
import {NewsService} from "../../models/services/news.service"
import {Router} from "@angular/router"

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {
  categories: Category[]

  constructor(private newsService: NewsService, private router: Router) { }

  ngOnInit() {

    this.newsService.fetchCategories().subscribe(categories => {
      this.categories = categories
    })
  }

  navigateToCategory(categoryId: string) {
    this.router.navigate(["category/" + categoryId])
  }
}
