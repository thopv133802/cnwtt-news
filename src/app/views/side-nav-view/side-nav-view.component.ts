import {Component, ElementRef, EventEmitter, OnInit, Output, ViewChild} from "@angular/core"
import {MatSidenav} from "@angular/material"
import {AuthService} from "../../models/services/auth.service"
import {UserRole} from "../../models/vos/auth.vo"
import {Router} from "@angular/router"

@Component({
  selector: 'app-side-nav-view',
  templateUrl: './side-nav-view.component.html',
  styleUrls: ['./side-nav-view.component.css']
})
export class SideNavViewComponent implements OnInit {
  currentUserFullName = "khách"
  isAdmin = false
  isAuth = false
  username: string
  @Output()
  navListClickListener = new EventEmitter<string>()
  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.authService.fetchCurrentUserInformation().subscribe(inform => {
      this.currentUserFullName = inform.fullName
      this.username = inform.username
      this.isAdmin = inform.isAdmin
      this.isAuth = inform.isAuth
    })
  }

  onNavListClicked(title: string) {
    this.navListClickListener.next(title)
  }

  navigateToProfile() {
    this.router.navigate(["profile/common/" + this.username])
  }
}
