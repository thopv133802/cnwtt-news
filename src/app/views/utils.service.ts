import { Injectable } from '@angular/core';
import {MatSnackBar, MatSnackBarRef, SimpleSnackBar} from "@angular/material"
import {Observable} from "rxjs"

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor(private snackbar: MatSnackBar) { }

  showSnackBar(message: string): MatSnackBarRef<SimpleSnackBar> {
    return this.snackbar.open(message, null, {
      duration: 2000
    })
  }
  showSnackBarWithAction(message: string, action: string): Observable<void>{
    return this.snackbar.open(message, action, {
      duration: 2000
    }).onAction()
  }
}
