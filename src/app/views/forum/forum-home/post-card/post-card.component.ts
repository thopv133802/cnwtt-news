import {Component, Input, OnInit} from "@angular/core"
import {Post} from "../../../../models/vos/post.vo"
import {PostService} from "../../../../models/services/post.service"
import {Router} from "@angular/router"

@Component({
  selector: 'app-post-card',
  templateUrl: './post-card.component.html',
  styleUrls: ['./post-card.component.css']
})
export class PostCardComponent implements OnInit {
  @Input()
  post: Post
  answerAmount = 0
  constructor(private postService: PostService, private router: Router) { }

  ngOnInit() {
    console.log(this.post)
    this.postService.fetchCountPostComments(this.post.id).subscribe(amount => {
      this.answerAmount = amount
    })
  }
  navigateToDetail(){
    this.router.navigate(["forum/post/" + this.post.id])
  }
}
