import { Component, OnInit } from '@angular/core';
import {Post} from "../../../models/vos/post.vo"
import {PostService} from "../../../models/services/post.service"
import {Router} from "@angular/router"

@Component({
  selector: 'app-forum-home',
  templateUrl: './forum-home.component.html',
  styleUrls: ['./forum-home.component.css']
})
export class ForumHomeComponent implements OnInit {
  posts: Post[]
  searchedPosts: Post[]
  showSearchResultLabel = false
  liveKeyword: string
  keyword: string

  constructor(private postService: PostService, private router: Router) { }

  ngOnInit() {
    this.postService.fetchPosts().subscribe(posts => {
      this.posts = posts
      this.requestSearch()
    })
  }
  requestSearch(){
    this.keyword = this.liveKeyword
    if (this.keyword == null || this.keyword.length === 0){
      this.searchedPosts = this.posts
    }
    else {
      this.searchedPosts = this.posts.filter(post => {
        return post.title.includes(this.keyword)
        || post.content.includes(this.keyword)
        || post.creatorId.includes(this.keyword)
      })
    }
  }

  keywordChange() {
    if (this.liveKeyword == null || this.liveKeyword.length === 0){
      this.requestSearch()
    }
  }

  navigateToCreatePost() {
    this.router.navigate(["forum/create-post"])
  }
}
