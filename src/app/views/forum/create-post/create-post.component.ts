import { Component, OnInit } from '@angular/core';
import {createNewPost, Post} from "../../../models/vos/post.vo"
import {PostService} from "../../../models/services/post.service"
import {UtilsService} from "../../utils.service"
import {Router} from "@angular/router"
import {AuthService} from "../../../models/services/auth.service"
import {first} from "rxjs/operators"

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.css']
})
export class CreatePostComponent implements OnInit {
  content: string[]
  title: string
  currentUserName: string
  currentTimeInMillis: number

  constructor(private authService: AuthService, private postService: PostService, private uiService: UtilsService, private router: Router) { }

  ngOnInit() {
    this.title = "Tiêu đề của bài viết đặt ở đây"
    this.content = [
      "Nội dung của bài viết đặt ở đây. Để sửa đổi nội dung, vui lòng bấm vào icon chỉnh sửa phía bên phải",
      "Nội dung của bài viết đặt ở đây. Để sửa đổi nội dung, vui lòng bấm vào icon chỉnh sửa phía bên phải",
      "Nội dung của bài viết đặt ở đây. Để sửa đổi nội dung, vui lòng bấm vào icon chỉnh sửa phía bên phải",
      "Nội dung của bài viết đặt ở đây. Để sửa đổi nội dung, vui lòng bấm vào icon chỉnh sửa phía bên phải",
      "Nội dung của bài viết đặt ở đây. Để sửa đổi nội dung, vui lòng bấm vào icon chỉnh sửa phía bên phải",
      "Nội dung của bài viết đặt ở đây. Để thêm nội dung mới, vui lòng bấm nút thêm nội dung mới phía dưới."
    ]
    this.currentTimeInMillis = Date.now()
    this.authService.fetchCurrentUserInformation().pipe(first())
      .subscribe(inform => {
        this.currentUserName = inform.fullName
      })
  }

  requestAddNewContent() {
    const length = this.content.length
    this.content[length] = "Nhấn vào icon chỉnh sửa phía bên phải để thay đổi nội dung"
  }

  removeContent(index: number) {
    this.content = [
      ...this.content.slice(0, index),
      ...this.content.slice(index + 1)
    ]
  }

  requestCreatePost() {
    this.postService.createPost({
      content: this.content,
      title: this.title
    }).subscribe(null, error => {
      this.uiService.showSnackBar(error)
    }, () => {
      this.uiService.showSnackBar("Đăng bài viết thành công, Bạn sẽ được điều hướng về trang diễn đàn ngay.").afterDismissed().subscribe(() => {
        this.router.navigate(["forum/forum-home"])
      })
    })
  }
}
