import {Component, Input, OnInit} from "@angular/core"
import {PostComment} from "../../../../models/vos/post.vo"

@Component({
  selector: 'app-post-comment-card',
  templateUrl: './post-comment-card.component.html',
  styleUrls: ['./post-comment-card.component.css']
})
export class PostCommentCardComponent implements OnInit {
  @Input()
  comment: PostComment
  constructor() { }

  ngOnInit() {
  }

}
