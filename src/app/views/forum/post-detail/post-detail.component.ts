import { Component, OnInit } from '@angular/core';
import {Post, PostComment} from "../../../models/vos/post.vo"
import {PostService} from "../../../models/services/post.service"
import {ActivatedRoute, Router} from "@angular/router"
import {flatMap} from "rxjs/operators"
import {UtilsService} from "../../utils.service"
import {NgForm} from "@angular/forms"
import {AuthService} from "../../../models/services/auth.service"

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.css']
})
export class PostDetailComponent implements OnInit {
  post: Post
  editable = false
  comments: PostComment[]
  isAuth = false
  newComment: string

  constructor(private router: Router, private route: ActivatedRoute, private postService: PostService, private uiService: UtilsService, private authService: AuthService) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.postService.increaseViewsAmount(params.get("id"))
      this.postService.fetchPost(params.get("id")).subscribe(post => {
        this.post = post
      })
      this.postService.fetchPost(params.get("id"))
        .pipe(
          flatMap(post => {
            return this.postService.currentUserEditable(post)
          })
        )
        .subscribe(editable => {
          this.editable = editable
        })
      this.postService.fetchComments(params.get("id")).subscribe(comments => {
        this.comments = comments
      })
    })
    this.authService.fetchIsAuth().subscribe(isAuth => {
      this.isAuth = isAuth
    })
  }

  requestUpdatePost() {
    this.postService.updatePost(this.post).subscribe(null, error => {
      this.uiService.showSnackBar(error)
    }, () => {
      this.uiService.showSnackBar("Cập nhật bài đăng thành công")
    })
  }

  addComment(addCommentForm: NgForm) {
    this.postService.addComment({
      content: this.newComment,
      postId: this.post.id
    }).subscribe(null, error => {
      this.uiService.showSnackBar(error)
    }, () => {
      this.uiService.showSnackBar("Thêm bình luận thành công")
      addCommentForm.resetForm()
    })
  }

  navigateToLogin() {
    this.router.navigate(["/login"])
  }

  removeContent(index: number) {
    this.post.content = [
      ...this.post.content.slice(0, index),
      ...this.post.content.slice(index + 1)
    ]
  }

  requestAddNewContent() {
    const contentLength = this.post.content.length
    this.post.content[contentLength] = "Bấm vào icon chỉnh sửa phía bên cạnh để thay đổi nội dung"
    this.requestUpdatePost()
  }

  updatePostContent(index: number, newContent: string) {
    this.post.content[index] = newContent
    this.requestUpdatePost()
  }
}
