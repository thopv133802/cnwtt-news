import {Injectable, NgModule} from "@angular/core"
import { CommonModule } from '@angular/common';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterModule, RouterStateSnapshot, Routes} from "@angular/router"
import {HomeComponent} from "./views/home/home.component"
import {LoginComponent} from "./views/login/login.component"
import {RegisterComponent} from "./views/register/register.component"
import {NewsDetailComponent} from "./views/news-detail/news-detail.component"
import {CategoryComponent} from "./views/category/category.component"
import {ChangePasswordGuard, IsAdminGuard, IsAuthGuard, IsNotAuthGuard} from "./app-routing.guard"
import {AdministrateComponent} from "./views/administrate/administrate.component"
import {NewsManagementComponent} from "./views/administrate/news-management/news-management.component"
import {EditNewsComponent} from "./views/administrate/news-management/edit-news/edit-news.component"
import {CreateNewsComponent} from "./views/administrate/news-management/create-news/create-news.component"
import {UserManagementComponent} from "./views/administrate/user-management/user-management.component"
import {UserManagementCreateUserComponent} from "./views/administrate/user-management/user-management-create-user/user-management-create-user.component"
import {LogoutComponent} from "./views/logout/logout.component"
import {ForumComponent} from "./views/forum/forum.component"
import {ForumHomeComponent} from "./views/forum/forum-home/forum-home.component"
import {PostDetailComponent} from "./views/forum/post-detail/post-detail.component"
import {ProfileComponent} from "./views/profile/profile.component"
import {CreatePostComponent} from "./views/forum/create-post/create-post.component"
import {UpdateProfileComponent} from "./views/profile/update-profile/update-profile.component"
import {ChangePasswordComponent} from "./views/profile/change-password/change-password.component"



export const routes: Routes = [
  {path: "", redirectTo: "/home", pathMatch: "full"},
  {path: "home", component: HomeComponent},
  {path: "login", component: LoginComponent, canActivate: [IsNotAuthGuard]},
  {path: "logout", component: LogoutComponent, canActivate: [IsAuthGuard]},
  {path: "register", component: RegisterComponent, canActivate: [IsNotAuthGuard]},
  {path: "news/:id", component: NewsDetailComponent},
  {path: "category/:id", component: CategoryComponent},
  {path: "profile", component: ProfileComponent, children: [
      {path: "common/:id", component: UpdateProfileComponent, pathMatch: "full"},
      {path: "change-password/:id", component: ChangePasswordComponent, canActivate: [ChangePasswordGuard]}
    ], canActivate: [IsAuthGuard]},
  {
    path: "administrate",
    component: AdministrateComponent,
    children: [
      {path: "", redirectTo: "news-management", pathMatch: "full"},
      {path: "news-management", component: NewsManagementComponent},
      {path: "news-management/edit/:id", component: EditNewsComponent},
      {path: "news-management/create", component: CreateNewsComponent},
      {path: "user-management", component: UserManagementComponent},
      {path: "user-management/create", component: UserManagementCreateUserComponent},
    ],
    canActivate: [IsAdminGuard]
  },
  {path: "forum", component: ForumComponent, canActivate: [IsAuthGuard], children: [
      {path: "", redirectTo: "forum-home", pathMatch: "full"},
      {path: "forum-home", component: ForumHomeComponent},
      {path: "post/:id", component: PostDetailComponent},
      {path: "create-post", component: CreatePostComponent}
    ]},
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes, {
      onSameUrlNavigation: "reload"
    })
  ],
  exports: [
    RouterModule
  ],
  providers: [IsNotAuthGuard, IsAuthGuard, IsAdminGuard, ChangePasswordGuard]
})
export class AppRoutingModule { }

