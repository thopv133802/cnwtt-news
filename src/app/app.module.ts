import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './models/reducers';
import { HeaderComponent } from './views/header/header.component';

import {MatCheckboxModule} from '@angular/material';
import {MatButtonModule} from '@angular/material';
import {MatInputModule} from '@angular/material/input';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import {MatSliderModule} from '@angular/material/slider';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatMenuModule} from '@angular/material/menu';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatListModule} from '@angular/material/list';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatCardModule} from '@angular/material/card';
import {MatStepperModule} from '@angular/material/stepper';
import {MatTabsModule} from '@angular/material/tabs';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatChipsModule} from '@angular/material/chips';
import {MatIconModule} from '@angular/material/icon';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatDialogModule} from '@angular/material/dialog';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatPaginatorModule} from '@angular/material/paginator';
import {FlexLayoutModule} from "@angular/flex-layout";
import {AppRoutingModule} from "./app-routing.module";
import { HomeComponent } from './views/home/home.component';
import { FirstComponent } from './views/home/first/first.component';
import { RestComponent } from './views/home/rest/rest.component';
import { FirstNewsCardComponent } from './views/home/first/first-news-card/first-news-card.component';
import { RestNewsCardComponent } from './views/home/rest/rest-news-card/rest-news-card.component';
import { FooterComponent } from './views/footer/footer.component';
import { RestCatComponent } from './views/home/rest/rest-cat/rest-cat.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';
import {FormsModule} from "@angular/forms";
import { NewsDetailComponent } from './views/news-detail/news-detail.component';
import { ImageViewComponent } from './views/news-detail/image-view/image-view.component';
import {TimeAgoPipe} from "time-ago-pipe";
import { NewsDetailRestNewsCardComponent } from './views/news-detail/news-detail-rest-news-card/news-detail-rest-news-card.component';
import { CategoryComponent } from './views/category/category.component';
import { CategoryVerticalNewsComponent } from './views/category/category-vertical-news/category-vertical-news.component';
import { CategoryHorizontalNewsComponent } from './views/category/category-horizontal-news/category-horizontal-news.component';
import { NewsCommentCardComponent } from './views/news-detail/news-comment-card/news-comment-card.component';
import { NewsCommentsComponent } from './views/news-detail/news-comments/news-comments.component';
import { SideNavViewComponent } from './views/side-nav-view/side-nav-view.component';
import { LayoutModule } from '@angular/cdk/layout';
import {AdministrateDrawerComponent} from "./views/administrate/administrate-drawer/administrate-drawer.component"
import {NewsManagementComponent} from "./views/administrate/news-management/news-management.component"
import {AdministrateComponent} from "./views/administrate/administrate.component";
import { NewsManagementNewsCardComponent } from './views/administrate/news-management/news-management-news-card/news-management-news-card.component';
import { EditableTextComponent } from './views/administrate/news-management/edit-news/editable-text/editable-text.component';
import { EditNewsComponent } from './views/administrate/news-management/edit-news/edit-news.component';
import { EditableImageComponent } from './views/administrate/news-management/edit-news/editable-image/editable-image.component';
import { CreateNewsComponent } from './views/administrate/news-management/create-news/create-news.component';
import { UserManagementComponent } from './views/administrate/user-management/user-management.component';
import { UserManagementCardComponent } from './views/administrate/user-management/user-management-card/user-management-card.component';
import { UserManagementCreateUserComponent } from './views/administrate/user-management/user-management-create-user/user-management-create-user.component';
import { LogoutComponent } from './views/logout/logout.component';
import { ForumComponent } from './views/forum/forum.component';
import { PostDetailComponent } from './views/forum/post-detail/post-detail.component';
import { ForumHomeComponent } from './views/forum/forum-home/forum-home.component';
import { PostCardComponent } from './views/forum/forum-home/post-card/post-card.component';
import { PostCommentCardComponent } from './views/forum/post-detail/post-comment-card/post-comment-card.component';
import { ProfileComponent } from './views/profile/profile.component';
import { CreatePostComponent } from './views/forum/create-post/create-post.component';
import { CategoriesComponent } from './views/categories/categories.component';
import { ChangePasswordComponent } from './views/profile/change-password/change-password.component';
import { UpdateProfileComponent } from './views/profile/update-profile/update-profile.component';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    FirstComponent,
    RestComponent,
    FirstNewsCardComponent,
    RestNewsCardComponent,
    FooterComponent,
    RestCatComponent,
    LoginComponent,
    RegisterComponent,
    NewsDetailComponent,
    ImageViewComponent,
    TimeAgoPipe,
    NewsDetailRestNewsCardComponent,
    CategoryComponent,
    CategoryVerticalNewsComponent,
    CategoryHorizontalNewsComponent,
    NewsCommentCardComponent,
    NewsCommentsComponent,
    SideNavViewComponent,
    AdministrateDrawerComponent,
    NewsManagementComponent,
    AdministrateComponent,
    NewsManagementNewsCardComponent,
    EditableTextComponent,
    EditNewsComponent,
    EditableImageComponent,
    CreateNewsComponent,
    UserManagementComponent,
    UserManagementCardComponent,
    UserManagementCreateUserComponent,
    LogoutComponent,
    ForumComponent,
    PostDetailComponent,
    ForumHomeComponent,
    PostCardComponent,
    PostCommentCardComponent,
    ProfileComponent,
    CreatePostComponent,
    CategoriesComponent,
    ChangePasswordComponent,
    UpdateProfileComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    MatCheckboxModule,
    MatCheckboxModule,
    MatButtonModule,
    MatInputModule,
    MatAutocompleteModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatRadioModule,
    MatSelectModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatStepperModule,
    MatTabsModule,
    MatExpansionModule,
    MatButtonToggleModule,
    MatChipsModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatDialogModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    FlexLayoutModule,
    AppRoutingModule,
    FormsModule,
    LayoutModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule { }
